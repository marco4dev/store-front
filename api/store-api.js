import axios from "axios";

const HOST = "https://stark-shore-58830.herokuapp.com";

export default axios.create({ baseURL: HOST });
