import Head from "next/head";
import Link from "next/link";
import { Fragment, useRef } from "react";
import Layout from "../components/layout";
import ProductForm from "../components/product-form";

export default function AddProduct() {
  const form = useRef(null);

  return (
    <Fragment>
      <Head>
        <title>Scandiweb | Add Product</title>
      </Head>
      <Layout
        title="Add Product"
        actions={
          <Fragment>
            <button
              className="btn btn-success"
              onClick={(e) => form.current.submit()}
            >
              Save
            </button>
            <Link href="/">
              <a className="btn btn-secondary ms-3">Cancel</a>
            </Link>
          </Fragment>
        }
        content={
          <div className="row g-0">
            <div className="col-12 col-lg-7 col-xl-6">
              <ProductForm ref={form} />
            </div>
          </div>
        }
      />
    </Fragment>
  );
}
