import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
  return (
    <div className="container">
      <div className="row g-0 flex-column min-vh-100">
        <div className="col">
          <Component {...pageProps} />
        </div>
        <div className="col-auto">
          <footer className="p-4 mt-5 border-top text-center">
            <h5 className="">Scandiweb Test assignment</h5>
          </footer>
        </div>
      </div>
    </div>
  );
}

export default MyApp;
