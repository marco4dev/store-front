import Head from "next/head";
import Link from "next/link";
import { Fragment, useState, useEffect } from "react";
import Layout from "../components/layout";
import Product from "../components/product";
import storeApi from "../api/store-api";

export default function Home() {
  const [products, setProducts] = useState([]);
  const [deleteList, setDeleteList] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    storeApi
      .get("/products")
      .then((res) => {
        if (res.data) {
          setProducts(res.data.products);
          setError(null);
        }
      })
      .catch((err) => {
        let errMsg = err.message;
        if (err.response) errMsg = err.response.data.error;
        setError(errMsg);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  const onProductChecked = (id, value) => {
    if (value) {
      if (!deleteList.includes(id)) {
        setDeleteList([...deleteList, id]);
      }
    } else {
      setDeleteList(deleteList.filter((v) => v !== id));
    }
  };

  const deleteProducts = () => {
    setLoading(true);
    storeApi
      .delete("/products", {
        data: {
          ids: deleteList,
        },
      })
      .then((res) => {
        setProducts(res.data.products);
        setError(null);
      })
      .catch((err) => {
        let errMsg = err.message;
        if (err.response) errMsg = err.response.data.error;
        setError(errMsg);
      })
      .finally(() => {
        setLoading(false);
        setDeleteList([]);
      });
  };

  function renderError() {
    return <div className="alert alert-danger">{error}</div>;
  }

  function renderLoading() {
    return (
      <div className="d-flex justify-content-center">
        <div className="spinner-grow text-center" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      </div>
    );
  }

  function renderProducts() {
    if (loading) {
      return renderLoading();
    } else if (error) {
      return renderError();
    } else {
      return (
        <div className="row g-4 row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4">
          {products.map((product) => {
            return (
              <div key={product.sku} className="col">
                <Product onChecked={onProductChecked} product={product} />
              </div>
            );
          })}
        </div>
      );
    }
  }

  return (
    <Fragment>
      <Head>
        <title>Scandiweb | Products List</title>
      </Head>
      <Layout
        title="Product List"
        actions={
          <Fragment>
            <Link href="/add-product">
              <a className="btn btn-primary">ADD</a>
            </Link>
            <button
              id="delete-product-btn"
              className="ms-3 btn btn-danger"
              onClick={deleteProducts}
            >
              MASS DELETE
            </button>
          </Fragment>
        }
        content={renderProducts()}
      />
    </Fragment>
  );
}
