export default function Product(props) {
  const product = props.product;

  function renderSpecificProduct() {
    const rows = [];

    if (product.size) {
      rows.push(
        <div key="size" className="card-text">
          Size: {product.size} MB
        </div>
      );
    }

    if (product.weight) {
      rows.push(
        <div key="weight" className="card-text">
          Weight: {product.weight} KG
        </div>
      );
    }

    if (product.height && product.width && product.length) {
      rows.push(
        <div key="dimentions" className="card-text">
          Dimension: {product.height}x{product.width}x{product.length}
        </div>
      );
    }

    return rows;
  }

  return (
    <div className="card">
      <div className="card-header">
        <form>
          <div className="form-check">
            <input
              id={"delete-" + product.id}
              name="deleteProducts"
              value={product.id}
              type="checkbox"
              className="form-check-input delete-checkbox"
              onChange={(e) => props.onChecked(product.id, e.target.checked)}
            />
          </div>
        </form>
      </div>
      <div className="card-body text-center">
        <p className="card-text">{product.sku}</p>
        <p className="card-text">{product.name}</p>
        <p className="card-text">{product.price}$</p>
        {renderSpecificProduct()}
      </div>
    </div>
  );
}
