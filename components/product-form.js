import {
  forwardRef,
  Fragment,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import { useRouter } from "next/router";
import validator from "validator";
import storeApi from "../api/store-api";

export default forwardRef(function ProductForm(props, ref) {
  const [sku, setSku] = useState("");
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [type, setType] = useState("");
  const [size, setSize] = useState("");
  const [weight, setWeight] = useState("");
  const [width, setWidth] = useState("");
  const [height, setHeight] = useState("");
  const [length, setLength] = useState("");

  const [skuError, setSkuError] = useState(null);
  const [nameError, setNameError] = useState(null);
  const [priceError, setPriceError] = useState(null);
  const [typeError, setTypeError] = useState(null);
  const [sizeError, setSizeError] = useState(null);
  const [weightError, setWeightError] = useState(null);
  const [widthError, setWidthError] = useState(null);
  const [heightError, setHeightError] = useState(null);
  const [lengthError, setLengthError] = useState(null);

  const [isFormValid, setFormValid] = useState(false);

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const router = useRouter();
  const form = useRef(null);

  useImperativeHandle(ref, () => ({
    submit() {
      return submitForm();
    },
  }));

  function createProduct() {
    if (isFormValid) {
      setLoading(true);
      storeApi
        .post("/products", {
          sku,
          name,
          price,
          type,
          size,
          weight,
          width,
          height,
          length,
        })
        .then(() => {
          router.push("/");
        })
        .catch((err) => {
          let errMsg = err.message;
          if (err.response) errMsg = err.response.data.error;
          setError(errMsg);
          setLoading(false);
        });
    }
  }

  function submitForm() {
    validateSku();
    validateName();
    validatePrice();
    validateType();

    switch (type) {
      case "Dvd":
        validateSize();
        break;

      case "Book":
        validateWeight();
        break;

      case "Furniture":
        validateHeight();
        validateWidth();
        validateLength();
        break;
    }

    setFormValid(true);

    if (skuError || nameError || priceError || typeError) {
      return;
    } else {
      switch (type) {
        case "Dvd":
          if (sizeError) return;
          break;

        case "Book":
          if (weightError) return;
          break;

        case "Furniture":
          if (heightError || widthError || lengthError) return;
          break;
        default:
          return;
      }
    }

    setLoading(true);
    storeApi
      .post("/products", {
        sku,
        name,
        price,
        type,
        size,
        weight,
        width,
        height,
        length,
      })
      .then(() => {
        router.push("/");
      })
      .catch((err) => {
        let errMsg = err.message;
        if (err.response) errMsg = err.response.data.error;
        setError(errMsg);
        setLoading(false);
      });
  }

  function validateSku() {
    switch (false) {
      case !validator.isEmpty(sku):
        setSkuError("Required");
        return false;

      case validator.isLength(sku, { min: 3 }):
        setSkuError("Very small");

        return false;

      case validator.isAlphanumeric(sku):
        setSkuError("Use only letters and numbers");
        break;

      default:
        setSkuError(null);
        return true;
    }
  }

  function validateName() {
    switch (false) {
      case !validator.isEmpty(name):
        setNameError("Required");
        return false;

      case validator.isLength(name, { min: 3 }):
        setNameError("Very small");
        return false;

      default:
        setNameError(null);
        return true;
    }
  }

  function validatePrice() {
    switch (false) {
      case !validator.isEmpty(price):
        setPriceError("Required");
        return false;

      case validator.isFloat(price):
        setPriceError("Invalid number");
        return false;

      default:
        setPriceError(null);
        return true;
    }
  }

  function validateType() {
    switch (false) {
      case !validator.isEmpty(type):
        setTypeError("Required");
        return false;

      case validator.isIn(type, ["Dvd", "Book", "Furniture"]):
        setTypeError("Invalid type");

        return false;

      default:
        setTypeError(null);
        return true;
    }
  }

  function validateSize() {
    switch (false) {
      case !validator.isEmpty(size):
        setSizeError("Required");
        return false;

      case validator.isInt(size):
        setSizeError("Invalid number");
        return false;

      default:
        setSizeError(null);
        return true;
    }
  }

  function validateWeight() {
    switch (false) {
      case !validator.isEmpty(weight):
        setWeightError("Required");
        return false;

      case validator.isFloat(weight):
        setWeightError("Invalid number");
        return false;

      default:
        setWeightError(null);
        return true;
    }
  }

  function validateHeight() {
    switch (false) {
      case !validator.isEmpty(height):
        setHeightError("Required");
        return false;

      case validator.isFloat(height):
        setHeightError("Invalid number");
        return false;

      default:
        setHeightError(null);
        return true;
    }
  }

  function validateWidth() {
    switch (false) {
      case !validator.isEmpty(width):
        setWidthError("Required");
        return false;

      case validator.isFloat(width):
        setWidthError("Invalid number");
        return false;

      default:
        setWidthError(null);
        return true;
    }
  }

  function validateLength() {
    switch (false) {
      case !validator.isEmpty(length):
        setLengthError("Required");
        return false;

      case validator.isFloat(length):
        setLengthError("Invalid number");
        return false;

      default:
        setLengthError(null);
        return true;
    }
  }

  function renderDvdForm() {
    return (
      <div className="row mb-3">
        <label htmlFor="size" className="col-sm-3 col-form-label">
          Size (MB)
        </label>
        <div className="col-sm-9">
          <input
            id="size"
            type="text"
            className={"form-control" + (sizeError ? " is-invalid" : "")}
            value={size}
            onKeyUp={() => validateSize()}
            onChange={(e) => setSize(e.target.value)}
            aria-describedby="size-help"
          />
          <div className="invalid-feedback">{sizeError}</div>
          <p id="size-help" className="form-text mb-0">
            Please provide size in Megabytes
          </p>
        </div>
      </div>
    );
  }

  function renderBookForm() {
    return (
      <div className="row mb-3">
        <label htmlFor="weight" className="col-sm-3 col-form-label">
          Weight (KG)
        </label>
        <div className="col-sm-9">
          <input
            id="weight"
            type="text"
            className={"form-control" + (weightError ? " is-invalid" : "")}
            value={weight}
            onKeyUp={() => validateWeight()}
            onChange={(e) => setWeight(e.target.value)}
            aria-describedby="weight-help"
          />
          <div className="invalid-feedback">{weightError}</div>
          <p id="weight-help" className="form-text mb-0">
            Please provide weight in Kilograms
          </p>
        </div>
      </div>
    );
  }

  function renderFurnitureForm() {
    return (
      <Fragment>
        <div className="row mb-3">
          <label htmlFor="height" className="col-sm-3 col-form-label">
            Height (CM)
          </label>
          <div className="col-sm-9">
            <input
              id="height"
              type="text"
              className={"form-control" + (heightError ? " is-invalid" : "")}
              value={height}
              onKeyUp={() => validateHeight()}
              onChange={(e) => setHeight(e.target.value)}
              aria-describedby="height-help"
            />
            <div className="invalid-feedback">{heightError}</div>
            <p id="height-help" className="form-text mb-0">
              Please provide height in Centimetres
            </p>
          </div>
        </div>

        <div className="row mb-3">
          <label htmlFor="width" className="col-sm-3 col-form-label">
            Width (CM)
          </label>
          <div className="col-sm-9">
            <input
              id="width"
              type="text"
              className={"form-control" + (widthError ? " is-invalid" : "")}
              value={width}
              onKeyUp={() => validateWidth()}
              onChange={(e) => setWidth(e.target.value)}
              aria-describedby="width-help"
            />
            <div className="invalid-feedback">{widthError}</div>
            <p id="width-help" className="form-text mb-0">
              Please provide width in Centimetres
            </p>
          </div>
        </div>

        <div className="row mb-3">
          <label htmlFor="length" className="col-sm-3 col-form-label">
            Length (CM)
          </label>
          <div className="col-sm-9">
            <input
              id="length"
              type="text"
              className={"form-control" + (lengthError ? " is-invalid" : "")}
              value={length}
              onKeyUp={() => validateLength()}
              onChange={(e) => setLength(e.target.value)}
              aria-describedby="length-help"
            />
            <div className="invalid-feedback">{lengthError}</div>
            <p id="length-help" className="form-text mb-0">
              Please provide length in Centimetres
            </p>
          </div>
        </div>
      </Fragment>
    );
  }

  function renderProperForm() {
    switch (type) {
      case "Dvd":
        return renderDvdForm();
      case "Book":
        return renderBookForm();
      case "Furniture":
        return renderFurnitureForm();
      default:
        return;
    }
  }

  function renderError() {
    return error ? <div className="alert alert-danger">{error}</div> : "";
  }

  function renderLoading() {
    return (
      <div className="d-flex justify-content-center">
        <div className="spinner-grow text-center" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      </div>
    );
  }

  function renderForm() {
    return (
      <form id="product_form" noValidate ref={form}>
        {renderError()}
        <div className="row mb-3">
          <label htmlFor="sku" className="col-sm-3 col-form-label">
            SKU
          </label>
          <div className="col-sm-9">
            <input
              id="sku"
              type="text"
              className={"form-control" + (skuError ? " is-invalid" : "")}
              value={sku}
              onKeyUp={(e) => validateSku()}
              onChange={(e) => setSku(e.target.value)}
            />
            <div className="invalid-feedback">{skuError}</div>
          </div>
        </div>

        <div className="row mb-3">
          <label htmlFor="name" className="col-sm-3 col-form-label">
            Name
          </label>
          <div className="col-sm-9">
            <input
              id="name"
              type="text"
              className={"form-control" + (nameError ? " is-invalid" : "")}
              value={name}
              onKeyUp={() => validateName()}
              onChange={(e) => setName(e.target.value)}
            />
            <div className="invalid-feedback">{nameError}</div>
          </div>
        </div>

        <div className="row mb-3">
          <label htmlFor="price" className="col-sm-3 col-form-label">
            Price ($)
          </label>
          <div className="col-sm-9">
            <input
              id="price"
              type="text"
              className={"form-control" + (priceError ? " is-invalid" : "")}
              value={price}
              onKeyUp={() => validatePrice()}
              onChange={(e) => setPrice(e.target.value)}
            />
            <div className="invalid-feedback">{priceError}</div>
          </div>
        </div>

        <div className="row mb-3">
          <label htmlFor="productType" className="col-sm-3 col-form-label">
            Type Switcher
          </label>
          <div className="col-sm-9">
            <select
              id="productType"
              className={"form-select" + (typeError ? " is-invalid" : "")}
              aria-label="Product Type"
              value={type}
              onSelect={() => validateType()}
              onChange={(e) => setType(e.target.value)}
            >
              <option disabled value="">
                Select a Type
              </option>
              <option id="DVD" value="Dvd">
                DVD
              </option>
              <option id="Book" value="Book">
                Book
              </option>
              <option id="Furniture" value="Furniture">
                Furniture
              </option>
            </select>
            <div className="invalid-feedback">{typeError}</div>
          </div>
        </div>
        {renderProperForm()}
      </form>
    );
  }

  return loading ? renderLoading() : renderForm();
});
