export default function Layout(props) {
  return (
    <div id="layout">
      <header className="row g-0 my-3 pb-2 justify-content-between border-bottom">
        <div className="col-auto">
          <h2>{props.title}</h2>
        </div>
        <div className="col-auto">{props.actions}</div>
      </header>
      <main>{props.content}</main>
    </div>
  );
}
